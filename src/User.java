public class User {
    //[SECTION 1] PROPERTIES
    private String firstName;
    private String lastName;
    private Integer age;
    private String address;

    private Course kurso;

    //[SECTION 2] CONSTRUCTORS

    //default
    public User() {

    }

    public User(String firstName, String lastName, Integer age, String address, Course course) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.address = address;
        this.kurso = course;
    }
    //[SECTION 3] SETTERS
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public void setKurso(Course kurso) {
        this.kurso = kurso;
    }

    //[SECTION 4] GETTERS
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public Integer getAge() {
        return age;
    }
    public String getAddress() {
        return address;
    }

    public Course getKurso() {
        return kurso;
    }

    //[SECTION 5] METHODS
    public void info() {
        System.out.println("User's First Name: " + this.firstName);
        System.out.println("User's Last Name: " + this.lastName);
        System.out.println("User's Age: " + this.age);
        System.out.println("User's Address: " + this.address);
    }
    public String InstructorsFname() {
        return this.kurso.getInstructor();
    }

}
