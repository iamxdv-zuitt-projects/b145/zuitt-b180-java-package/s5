public class Main {

    /*
    1. In your s5 folder, Create a new Java Project named "a1"
    2. Create a Java Class named User.java with instance variables/properties for
        a. firstname- String
        b. lastname- String
        c. age- Int
        d. Address-String
    3. Create a Java Class named Course.java with instance variables/properties for
        a. name- String
        b. description- String
        c. seats- Int
        d. fee- Double
        e. startDate- String
        f. endDate- String
        g. instructor-String
    4. Define constructors (both default and parameterized, getters and setters for both classes.
    5. In the Main.java class, instantiate a new User object using the User class's parameterized class constructor.
    6. Instantiate a new Course object using the default constructor of the Course Class.

    7. Using the setters of the Course class, initialize every instance variable of the new course.
    8. Using the user object's getter methods print the values of its firstname, lastname, age and address properties.
    9. Using the course object's getter methods print the values of its name, description, fee, and instructors first name properties.
*/
    public static void main(String[] args) {
        Course course1 = new Course("Java Package", "An introduction to java", 30, 30000, "April 25, 2022",
                "May 15, 2022", "Tee Jae");
        User person1 = new User("Xyrus", "De Vera", 28, "Manila", course1);
        person1.info();

        //Print Out
        course1.courseInfo();
        System.out.println("Course Instructor's First Name: " + person1.InstructorsFname());

        //END OF SOLUTION
    }
}
