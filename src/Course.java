public class Course {

    //[SECTION 1] PROPERTIES
    private String name;
    private String description;
    private Integer seats;
    private double fee;
    private String startDate;
    private String endtDate;
    private String instructor;

    //[SECTION 2] CONSTRUCTORS
    public Course() {

    }
    public Course(String name, String description, Integer seats, double fee, String startDate, String endtDate, String instructor) {
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endtDate = endtDate;
        this.instructor = instructor;
    }
    //[SECTION 3] SETTERS

    public void setName(String name) {
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setSeats(Integer seats) {
        this.seats = seats;
    }
    public void setFee(double fee) {
        this.fee = fee;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    public void setEndtDate(String endtDate) {
        this.endtDate = endtDate;
    }
    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }
    //[SECTION 4] GETTERS

    public String getName() {
        return name;
    }
    public String getDescription() {
        return description;
    }
    public Integer getSeats() {
        return seats;
    }
    public double getFee() {
        return fee;
    }
    public String getStartDate() {
        return startDate;
    }
    public String getEndtDate() {
        return endtDate;
    }
    public String getInstructor() {
        return instructor;
    }

    //[SECTION 5] METHODS
    public void courseInfo() {
        System.out.println("Course Name: " + this.name);
        System.out.println("Course Description: " + this.description);
        System.out.println("Course Seats: " + this.seats);

    }





}
